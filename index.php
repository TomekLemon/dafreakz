<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf8">
        <title>DaFreakz / That's what we roll 4</title>
		<meta name="description" content="DaFreakz / That's what we roll 4 - zobacz trailer" />
		<link rel="stylesheet" href="assets/css/default.css" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" type="image/png" href="favicon.png?ver=6">
    </head>
    <body>
		<div id="content">
			<div class="container-fluid">
				<header class="header clearfix">
					<div class="brand pull-left">
						<a href="http://lemonadestudio.pl/" target="_blank">
							<img src="assets/images/logo.png" alt="logo" />
						</a>
					</div>
					<div class="social-media pull-right">
						<ul class="unstyled">
							<li>
								<a href="https://www.instagram.com/dafreakzcom/" class="hover" target="_blank">
									<img class="normal-state" src="assets/images/instagram-logo.png" alt="instagram" />
									<img class="hover-state" src="assets/images/instagram-logo-hover.png" alt="instagram" />
								</a>
							</li>
							<li>
								<a href="https://www.facebook.com/DaFreakzCom/?fref=ts" class="hover" target="_blank">
									<img class="normal-state" src="assets/images/facebook-logo.png" alt="facebook" />
									<img class="hover-state" src="assets/images/facebook-logo-hover.png" alt="instagram" />
								</a>
							</li>
						</ul>
					</div>
				</header>
				<div class="content">
					<div class="logos">
						<img class="main-image" src="assets/images/logos.png" alt="dafreakz that's what we roll 4" />
						<img class="small-image" src="assets/images/logos-small.png" alt="dafreakz" />
					</div>
					<div class="trailer-button">
						<a href="https://www.youtube.com/watch?v=mkSLsTxIukA" class="hover" target="_blank">
							<img class="normal-state" src="assets/images/play-btn.png" alt="zobacz trailer" />
							<img class="hover-state" src="assets/images/play-btn-hover.png" alt="zobacz trailer" />
						</a>
						<p>Zobacz film</p>
					</div>
				</div>
			</div>
			<div class="footer">
				<ul class="unstyled inline">
					<li>
						<a href="http://mtuning.pl/" target="_blank">
							<img src="assets/images/tuning.png" alt="tuning.pl" />
						</a>
					</li>
					<li>
						<a href="http://www.onyxx.pl/" target="_blank">
							<img src="assets/images/onyxx.png" alt="onyxx" />
						</a>
					</li>
					<li>
						<a href="http://www.maxtondesign.eu/" target="_blank">
							<img src="assets/images/maxton.png" alt="maxton design" />
						</a>
					</li>
					<li>
						<a href="http://www.wbline.pl/" target="_blank">
							<img src="assets/images/line.png" alt="line" />
						</a>
					</li>
					<li>
						<a href="http://www.lostark.pl/" target="_blank">
							<img src="assets/images/lostark.png" alt="lo-stark" />
						</a>
					</li>
					<li>
						<a href="http://cp-tuning.pl/" target="_blank">
							<img src="assets/images/cp-tuning.png" alt="cp-tuning" />
						</a>
					</li>
					<li>
						<a href="http://niebiesky.com/" target="_blank">
							<img src="assets/images/niebiesky.png" alt="niebiesky.com" />
						</a>
					</li>
				</ul>
			</div>
		</div>
		<script src="assets/js/jquery-3.1.1.min.js"></script>
		<script src="assets/js/jquery.bgswitcher.js"></script>
		<script src="assets/js/scripts.js?ver=2"></script>
    </body>
</html>
